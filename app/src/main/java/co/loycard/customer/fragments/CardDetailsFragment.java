package co.loycard.customer.fragments;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.util.ArrayList;

import co.loycard.customer.CustomerActivity;
import co.loycard.customer.PunchActivity;
import co.loycard.customer.R;
import co.loycard.customer.data_models.LoyaltyCard;
import co.loycard.customer.data_models.LoyaltyOffer;
import co.loycard.customer.data_models.LoyaltyReward;
import co.loycard.customer.data_models.Vendor;
import co.loycard.customer.observables.RxFirebase;
import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.Action;
import io.reactivex.functions.Consumer;
import io.reactivex.functions.Function3;

import static com.facebook.FacebookSdk.getApplicationContext;

/**
 * Created by Sam on 20/05/2017.
 */

public class CardDetailsFragment extends Fragment {

    private static final String TAG = "CardDetailsFragment";
    String dealNameString, vendorNameString,vendorAddressString,dealId;

    int ppr = 0;
    int purchaseCountInt = 0;
    int pc = 0;
    int ptnr = 0;
    int numberOfPurchaseToshow = 0 ;
    // The database key of the card to view
    private String mKey;
    // the card currently being viewed
    private LoyaltyCard mCard;
    // Firebase root database reference
    private DatabaseReference mRootRef;
    Query query;
    private StorageReference mStorage;
    DatabaseReference cardsReference;
    private FirebaseAuth mFirebaseAuth;
    int rewardsIssuedInt = 0;

    ImageView image1,image2,image3,image4,image5,image6,image7,image8,image9,image10,image11,image12;

    RelativeLayout rewardLayout;
    TextView rewardInfo;
    String cardID;

    // Gets a new instance and passes it the database key of the card to view
    public static Fragment newInstance(String key) {
        CardDetailsFragment cardDetailsFragment = new CardDetailsFragment();

        Bundle bundle = new Bundle();
        bundle.putString("Key", key);
        cardDetailsFragment.setArguments(bundle);

        return cardDetailsFragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mKey = getArguments().getString("Key");
        mFirebaseAuth = FirebaseAuth.getInstance();
        mStorage = FirebaseStorage.getInstance().getReference();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_card_details, container, false);
        view.setTag(TAG);


       Toolbar toolbar = (Toolbar) getActivity().findViewById(R.id.toolbar);

        TextView  title =  (TextView) toolbar.findViewById(R.id.appTitle);

        title.setText("My Card");


        // Get loyaltycards database reference
          cardsReference = FirebaseDatabase.getInstance().getReference().child("LoyaltyCards");
        mRootRef = FirebaseDatabase.getInstance().getReference();

        // Create a reference with an initial file path and name


        attachDatabaseListner();

        return view;
    }


    public void attachDatabaseListner(){
        query = cardsReference.orderByKey().equalTo(mKey);
        ValueEventListener listener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {
                    for (DataSnapshot cardSnapshot : dataSnapshot.getChildren()) {
                        mCard = cardSnapshot.getValue(LoyaltyCard.class);
                        mCard.setCardID(cardSnapshot.getKey());
                    }

                    //offerID = mCard.offerID;
                    cardID = mCard.retrieveCardID();

                    // Log.e("offerID",offerID);
                    Log.e(TAG, "Data chnaged");
                    populateDetailView(mCard);


                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.d(TAG, "onCancelled: datbase error occured");
                attachDatabaseListner();
            }
        };
        query.addListenerForSingleValueEvent(listener);
    }

    private void populateDetailView(final LoyaltyCard loyaltyCard) {
        Observable<Vendor> ven = RxFirebase.getVendor(mRootRef, loyaltyCard.vendorID);
        Observable<LoyaltyOffer> off = RxFirebase.getLoyaltyOffer(mRootRef, loyaltyCard.offerID);
        Observable<LoyaltyCard> card = Observable.just(loyaltyCard);
        Function3<Vendor, LoyaltyOffer, LoyaltyCard, LoyaltyCard> f = new Function3<Vendor, LoyaltyOffer, LoyaltyCard, LoyaltyCard>() {
            @Override
            public LoyaltyCard apply(@io.reactivex.annotations.NonNull Vendor vendor, @io.reactivex.annotations.NonNull LoyaltyOffer offer, @io.reactivex.annotations.NonNull LoyaltyCard loyaltyCard) throws Exception {
                Log.d(TAG, "applying offer/vendor");

                loyaltyCard.setVendor(vendor);
                loyaltyCard.setOffer(offer);

                return loyaltyCard;
            }
        };

        // zip the two observable streams together and returns a new observable
        Observable<LoyaltyCard> observable = Observable.zip(ven, off, card, f);

        observable.retry(3)
                .doOnError(new Consumer<Throwable>() {
                    @Override
                    public void accept(@io.reactivex.annotations.NonNull Throwable throwable) throws Exception {
                        Log.d(TAG, "MyCardsActivity: Observable from iterable: " + throwable.getMessage());
                    }
                }).onErrorReturnItem(new LoyaltyCard())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnComplete(new Action() {
                    @Override
                    public void run() throws Exception {
                        // set the cards to the recycler adapter
                        Log.d(TAG, "doOnComplete filling card details");
                        attachRewardListener();
                        fillCardDetails(loyaltyCard);

                    }
                })
                .subscribe();
    }


    private void fillCardDetails(final LoyaltyCard card) {
        Activity activity = getActivity();

        ProgressBar spinner = (ProgressBar) activity.findViewById(R.id.detail_spinner);


        TextView businessName = (TextView) activity.findViewById(R.id.detail_bizName);
        TextView businessAddress = (TextView) activity.findViewById(R.id.detail_bizAddr);
        TextView offerDescription = (TextView) activity.findViewById(R.id.detail_offerDesc);
        TextView purchasesPerReward = (TextView) activity.findViewById(R.id.detail_ppr);
        TextView reward = (TextView) activity.findViewById(R.id.detail_reward);
        TextView purchaseCount = (TextView) activity.findViewById(R.id.detail_purchCount);
        TextView purchasesToNextReward = (TextView) activity.findViewById(R.id.detail_purchToNext);
        final Button button = (Button) activity.findViewById(R.id.button);
        TextView rewardsIssued = (TextView) activity.findViewById(R.id.reward_issued);
//        TextView rewardsClaimed = (TextView) activity.findViewById(R.id.detail_rewardsClaimed);
        final ImageView imageView = (ImageView) activity.findViewById(R.id.product_image);


           image1 = (ImageView) activity.findViewById(R.id.image1);
           image2 = (ImageView) activity.findViewById(R.id.image2);
           image3 = (ImageView) activity.findViewById(R.id.image3);
           image4 = (ImageView) activity.findViewById(R.id.image4);
           image5 = (ImageView) activity.findViewById(R.id.image5);
           image6 = (ImageView) activity.findViewById(R.id.image6);
           image7 = (ImageView) activity.findViewById(R.id.image7);
           image8 = (ImageView) activity.findViewById(R.id.image8);
           image9 = (ImageView) activity.findViewById(R.id.image9);
           image10 = (ImageView) activity.findViewById(R.id.image10);
           image11 = (ImageView) activity.findViewById(R.id.image11);
           image12 = (ImageView) activity.findViewById(R.id.image12);
           rewardInfo = (TextView) activity.findViewById(R.id.info);
        rewardLayout = (RelativeLayout) activity.findViewById(R.id.rewardLayout);




//        rewardLayout.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                ((CustomerActivity)getActivity()).openReward();
//            }
//        });




        spinner.setVisibility(View.VISIBLE);

        mStorage.child("Images/" +card.offerID.toString()+ "/" +card.vendorID.toString()+ "/").getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
            @Override
            public void onSuccess(Uri uri) {
                Glide.with(getApplicationContext()).load(uri).override(100,100).error(R.drawable.placeholder).into(imageView);

            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception exception) {
                Glide.with(getApplicationContext()).load(R.drawable.placeholder).override(100,100).into(imageView);
            }
        });

        vendorNameString = card.retrieveVendor().businessName ;
        vendorAddressString = card.retrieveVendor().businessAddress ;
        dealNameString = card.retrieveOffer().description ;




        businessName.setText(card.retrieveVendor().businessName);
        businessAddress.setText(card.retrieveVendor().businessAddress);
        offerDescription.setText(card.retrieveOffer().description);
        purchasesPerReward.setText("Purchases Per Reward: " + card.retrieveOffer().purchasesPerReward);
        reward.setText("Reward: " + card.retrieveOffer().reward);
        purchaseCount.setText("Purchases Count: " + card.purchaseCount);
        purchaseCountInt = Integer.parseInt(card.purchaseCount);
          ppr = Integer.parseInt(card.retrieveOffer().purchasesPerReward);
          pc = Integer.parseInt(card.purchaseCount);
          ptnr = ppr - pc % ppr;
        purchasesToNextReward.setText("Purchases to Next Reward: " + ptnr);
        rewardsIssued.setText("Rewards Issued: " + card.rewardsIssued);
//        rewardsClaimed.setText("Rewards Claimed: " + card.rewardsClaimed);

        rewardsIssuedInt = Integer.parseInt(card.rewardsIssued);
        spinner.setVisibility(View.GONE);


     //   Log.e("foo",rewardsIssuedInt + "  not null");
    //    Log.e("offerID_customerID",card.offerID_customerID + "  not null");

        int ri = Integer.parseInt(card.rewardsIssued);

          numberOfPurchaseToshow = pc % 12;
        int rewardOfPurchaseToshow = ri % 12;





            button.setText("Punch now");



     //   final boolean finalPendingReward = pendingReward;
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {



                    String text = button.getText().toString();

                    Intent intent = new Intent(getApplicationContext(),
                            PunchActivity.class);
                    intent.putExtra("dealNameString", dealNameString);
                    intent.putExtra("vendorNameString", vendorNameString);
                    intent.putExtra("vendorAddressString", vendorAddressString);
                    intent.putExtra("token", text);
                    intent.putExtra("offerID_customerID", mCard.offerID_customerID);
                    intent.putExtra("purchaseCount", Integer.parseInt(mCard.purchaseCount));
                    intent.putExtra("rewardCount", rewardsIssuedInt);


                    startActivity(intent);



            }
        });



        showPurchase();

        checkNextReward();


      // checkPunchedReward();
    }


    private Query mQueryAward;

 //   private DatabaseReference mLoyaltyRewardsRef;
    private ValueEventListener mValueEventListener;

    DatabaseReference mLoyaltyRewardsRef = FirebaseDatabase.getInstance().getReference().child("LoyaltyRewards");

    private void attachRewardListener() {

        String   mUserID = FirebaseAuth.getInstance().getCurrentUser().getUid();
        String   cardID_customerID = cardID + "_"+mUserID;

        Log.e("cardID_customerID",cardID_customerID);

       // mQueryAward = mLoyaltyRewardsRef.orderByChild("customerID").equalTo(mUserID).orderByChild("offerID").equalTo();
        mQueryAward = mLoyaltyRewardsRef.orderByChild("cardID_customerID").equalTo(cardID_customerID);
     //   Log.e(TAG, "Current mUid: " + offerID_customerID);

        if (mValueEventListener == null) {
            mValueEventListener = new ValueEventListener() {

                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    Log.d(TAG, "onDataChange started");
                    if (dataSnapshot.exists()) {
                        Log.d(TAG, "onDataChange: data change detected");

                        final ArrayList<LoyaltyReward> rewards = new ArrayList<>();

                        for (DataSnapshot rewardSnapshot : dataSnapshot.getChildren()) {
                            LoyaltyReward reward = rewardSnapshot.getValue(LoyaltyReward.class);
                            reward.setRewardID(rewardSnapshot.getKey());
                            rewards.add(0, reward);
                        }

                        int size = rewards.size();

                        Log.e("size",size+"");

                        if(size>0){
                            showNumberOfReward(size);
                        }

                    } else {
                        // Removes
                        Log.d(TAG, "dataSnapshot doesn't exist");

                    }
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {
                    attachRewardListener();
                }
            };
        } else {
            Log.d(TAG, "mValueEventListener is not null");
        }
        Log.d(TAG, "addValueEventListener added to mQuery");
        mQueryAward.addValueEventListener(mValueEventListener);
    }


    public void showNumberOfReward(int number){

        rewardLayout.setVisibility(View.VISIBLE);
        if(number == 1) {
            rewardInfo.setText("You have " + number + " pending reward");
        }else{
            rewardInfo.setText("You have " + number + " pending rewards");
        }

    }

    @Override
    public void onResume() {
        attachDatabaseListner();
        super.onResume();
    }

    public void showPurchase(){
        int x = purchaseCountInt % ppr;

        switch (x) {
            case 0:
                image1.setImageResource(R.drawable.ic_empty);
                image2.setImageResource(R.drawable.ic_empty);
                image3.setImageResource(R.drawable.ic_empty);
                image4.setImageResource(R.drawable.ic_empty);
                image5.setImageResource(R.drawable.ic_empty);
                image5.setImageResource(R.drawable.ic_empty);
                image6.setImageResource(R.drawable.ic_empty);
                image7.setImageResource(R.drawable.ic_empty);
                image8.setImageResource(R.drawable.ic_empty);
                image9.setImageResource(R.drawable.ic_empty);
                image10.setImageResource(R.drawable.ic_empty);
                image11.setImageResource(R.drawable.ic_empty);
                image12.setImageResource(R.drawable.ic_empty);

                break;
            case 1:
                image1.setImageResource(R.drawable.ic_punched);
                image2.setImageResource(R.drawable.ic_empty);
                image3.setImageResource(R.drawable.ic_empty);
                image4.setImageResource(R.drawable.ic_empty);
                image5.setImageResource(R.drawable.ic_empty);
                image5.setImageResource(R.drawable.ic_empty);
                image6.setImageResource(R.drawable.ic_empty);
                image7.setImageResource(R.drawable.ic_empty);
                image8.setImageResource(R.drawable.ic_empty);
                image9.setImageResource(R.drawable.ic_empty);
                image10.setImageResource(R.drawable.ic_empty);
                image11.setImageResource(R.drawable.ic_empty);
                image12.setImageResource(R.drawable.ic_empty);

                break;
            case 2:
                image1.setImageResource(R.drawable.ic_punched);
                image2.setImageResource(R.drawable.ic_punched);
                image3.setImageResource(R.drawable.ic_empty);
                image4.setImageResource(R.drawable.ic_empty);
                image5.setImageResource(R.drawable.ic_empty);
                image5.setImageResource(R.drawable.ic_empty);
                image6.setImageResource(R.drawable.ic_empty);
                image7.setImageResource(R.drawable.ic_empty);
                image8.setImageResource(R.drawable.ic_empty);
                image9.setImageResource(R.drawable.ic_empty);
                image10.setImageResource(R.drawable.ic_empty);
                image11.setImageResource(R.drawable.ic_empty);
                image12.setImageResource(R.drawable.ic_empty);
                break;
            case 3:
                image1.setImageResource(R.drawable.ic_punched);
                image2.setImageResource(R.drawable.ic_punched);
                image3.setImageResource(R.drawable.ic_punched);
                image4.setImageResource(R.drawable.ic_empty);
                image5.setImageResource(R.drawable.ic_empty);
                image5.setImageResource(R.drawable.ic_empty);
                image6.setImageResource(R.drawable.ic_empty);
                image7.setImageResource(R.drawable.ic_empty);
                image8.setImageResource(R.drawable.ic_empty);
                image9.setImageResource(R.drawable.ic_empty);
                image10.setImageResource(R.drawable.ic_empty);
                image11.setImageResource(R.drawable.ic_empty);
                image12.setImageResource(R.drawable.ic_empty);

                break;

            case 4:
                image1.setImageResource(R.drawable.ic_punched);
                image2.setImageResource(R.drawable.ic_punched);
                image3.setImageResource(R.drawable.ic_punched);
                image4.setImageResource(R.drawable.ic_punched);
                image5.setImageResource(R.drawable.ic_empty);
                image5.setImageResource(R.drawable.ic_empty);
                image6.setImageResource(R.drawable.ic_empty);
                image7.setImageResource(R.drawable.ic_empty);
                image8.setImageResource(R.drawable.ic_empty);
                image9.setImageResource(R.drawable.ic_empty);
                image10.setImageResource(R.drawable.ic_empty);
                image11.setImageResource(R.drawable.ic_empty);
                image12.setImageResource(R.drawable.ic_empty);

                break;

            case 5:
                image1.setImageResource(R.drawable.ic_punched);
                image2.setImageResource(R.drawable.ic_punched);
                image3.setImageResource(R.drawable.ic_punched);
                image4.setImageResource(R.drawable.ic_punched);
                image5.setImageResource(R.drawable.ic_punched);
                image5.setImageResource(R.drawable.ic_empty);
                image6.setImageResource(R.drawable.ic_empty);
                image7.setImageResource(R.drawable.ic_empty);
                image8.setImageResource(R.drawable.ic_empty);
                image9.setImageResource(R.drawable.ic_empty);
                image10.setImageResource(R.drawable.ic_empty);
                image11.setImageResource(R.drawable.ic_empty);
                image12.setImageResource(R.drawable.ic_empty);

                break;


            case 6:
                image1.setImageResource(R.drawable.ic_punched);
                image2.setImageResource(R.drawable.ic_punched);
                image3.setImageResource(R.drawable.ic_punched);
                image4.setImageResource(R.drawable.ic_punched);
                image5.setImageResource(R.drawable.ic_punched);
                image5.setImageResource(R.drawable.ic_punched);
                image6.setImageResource(R.drawable.ic_punched);
                image7.setImageResource(R.drawable.ic_empty);
                image8.setImageResource(R.drawable.ic_empty);
                image9.setImageResource(R.drawable.ic_empty);
                image10.setImageResource(R.drawable.ic_empty);
                image11.setImageResource(R.drawable.ic_empty);
                image12.setImageResource(R.drawable.ic_empty);

                break;

            case 7:
                image1.setImageResource(R.drawable.ic_punched);
                image2.setImageResource(R.drawable.ic_punched);
                image3.setImageResource(R.drawable.ic_punched);
                image4.setImageResource(R.drawable.ic_punched);
                image5.setImageResource(R.drawable.ic_punched);
                image5.setImageResource(R.drawable.ic_punched);
                image6.setImageResource(R.drawable.ic_punched);
                image7.setImageResource(R.drawable.ic_punched);
                image8.setImageResource(R.drawable.ic_empty);
                image9.setImageResource(R.drawable.ic_empty);
                image10.setImageResource(R.drawable.ic_empty);
                image11.setImageResource(R.drawable.ic_empty);
                image12.setImageResource(R.drawable.ic_empty);

                break;

            case 8:
                image1.setImageResource(R.drawable.ic_punched);
                image2.setImageResource(R.drawable.ic_punched);
                image3.setImageResource(R.drawable.ic_punched);
                image4.setImageResource(R.drawable.ic_punched);
                image5.setImageResource(R.drawable.ic_punched);
                image5.setImageResource(R.drawable.ic_punched);
                image6.setImageResource(R.drawable.ic_punched);
                image7.setImageResource(R.drawable.ic_punched);
                image8.setImageResource(R.drawable.ic_punched);
                image9.setImageResource(R.drawable.ic_empty);
                image10.setImageResource(R.drawable.ic_empty);
                image11.setImageResource(R.drawable.ic_empty);
                image12.setImageResource(R.drawable.ic_empty);

                break;

            case 9:
                image1.setImageResource(R.drawable.ic_punched);
                image2.setImageResource(R.drawable.ic_punched);
                image3.setImageResource(R.drawable.ic_punched);
                image4.setImageResource(R.drawable.ic_punched);
                image5.setImageResource(R.drawable.ic_punched);
                image5.setImageResource(R.drawable.ic_punched);
                image6.setImageResource(R.drawable.ic_punched);
                image7.setImageResource(R.drawable.ic_punched);
                image8.setImageResource(R.drawable.ic_punched);
                image9.setImageResource(R.drawable.ic_punched);
                image10.setImageResource(R.drawable.ic_empty);
                image11.setImageResource(R.drawable.ic_empty);
                image12.setImageResource(R.drawable.ic_empty);

                break;


            case 10:
                image1.setImageResource(R.drawable.ic_punched);
                image2.setImageResource(R.drawable.ic_punched);
                image3.setImageResource(R.drawable.ic_punched);
                image4.setImageResource(R.drawable.ic_punched);
                image5.setImageResource(R.drawable.ic_punched);
                image5.setImageResource(R.drawable.ic_punched);
                image6.setImageResource(R.drawable.ic_punched);
                image7.setImageResource(R.drawable.ic_punched);
                image8.setImageResource(R.drawable.ic_punched);
                image9.setImageResource(R.drawable.ic_punched);
                image10.setImageResource(R.drawable.ic_punched);
                image11.setImageResource(R.drawable.ic_empty);
                image12.setImageResource(R.drawable.ic_empty);

                break;

            case 11:
                image1.setImageResource(R.drawable.ic_punched);
                image2.setImageResource(R.drawable.ic_punched);
                image3.setImageResource(R.drawable.ic_punched);
                image4.setImageResource(R.drawable.ic_punched);
                image5.setImageResource(R.drawable.ic_punched);
                image5.setImageResource(R.drawable.ic_punched);
                image6.setImageResource(R.drawable.ic_punched);
                image7.setImageResource(R.drawable.ic_punched);
                image8.setImageResource(R.drawable.ic_punched);
                image9.setImageResource(R.drawable.ic_punched);
                image10.setImageResource(R.drawable.ic_punched);
                image11.setImageResource(R.drawable.ic_punched);
                image12.setImageResource(R.drawable.ic_empty);

                break;
            default:
                break;
        }
    }

    public void checkNextReward(){

        Log.e("awr",(ptnr)+"");
      //  if(ptnr==1)


            switch (ppr) {

                case 1:
                    image1.setImageResource(R.drawable.ic_reward);
                    break;
                case 2:
                    image2.setImageResource(R.drawable.ic_reward);
                    break;
                case 3:
                    image3.setImageResource(R.drawable.ic_reward);
                    break;
                case 4:
                    image4.setImageResource(R.drawable.ic_reward);
                    break;
                case 5:
                    image5.setImageResource(R.drawable.ic_reward);
                    break;
                case 6:
                    image6.setImageResource(R.drawable.ic_reward);
                    break;
                case 7:
                    image7.setImageResource(R.drawable.ic_reward);
                    break;
                case 8:
                    image8.setImageResource(R.drawable.ic_reward);
                    break;
                case 9:
                    image9.setImageResource(R.drawable.ic_reward);
                    break;
                case 10:
                    image10.setImageResource(R.drawable.ic_reward);
                    break;
                case 11:
                    image11.setImageResource(R.drawable.ic_reward);
                    break;
                case 12:
                    image12.setImageResource(R.drawable.ic_reward);
                    break;
                default:
                    break;
            }
    }



    public void checkPunchedReward(){

        ArrayList<Boolean> awardChecked = new ArrayList();

        for (int i =1;i<=purchaseCountInt;i++){
            if(i % ppr==1){
                if(i>ppr) {
                    awardChecked.add(true);
                }else{
                    awardChecked.add(false);
                }
            }else{
                awardChecked.add(false);
            }
        }


        for(int x =0;x<purchaseCountInt;x++){

            Log.e("x" ,  awardChecked.get(x)+"");
        }

        int x = purchaseCountInt % 12;
        int y = 0;
        if(x>purchaseCountInt) {
              y = purchaseCountInt - x;
        }else{
            y = x;
        }

        Log.e("x",x+"");
        Log.e("y",y+"");

        if(y>0) {

            try {



                if (awardChecked.get(y - 1)) {
                    image11.setImageResource(R.drawable.ic_award_punch);
                } if (awardChecked.get(y - 2)) {
                    image10.setImageResource(R.drawable.ic_award_punch);
                }  if (awardChecked.get(y - 3)) {
                    image9.setImageResource(R.drawable.ic_award_punch);
                }  if (awardChecked.get(y - 4) ) {
                    image8.setImageResource(R.drawable.ic_award_punch);
                }  if (awardChecked.get(y - 5)) {
                    image7.setImageResource(R.drawable.ic_award_punch);
                }  if (awardChecked.get(y - 6)) {
                    image6.setImageResource(R.drawable.ic_award_punch);
                }  if (awardChecked.get(y - 7)) {
                    image5.setImageResource(R.drawable.ic_award_punch);
                }  if (awardChecked.get(y - 8)) {
                    image4.setImageResource(R.drawable.ic_award_punch);
                }  if (awardChecked.get(y - 9)) {
                    image3.setImageResource(R.drawable.ic_award_punch);
                }  if (awardChecked.get(y - 10) ) {
                    image2.setImageResource(R.drawable.ic_award_punch);
                }  if (awardChecked.get(y - 11)) {
                    image1.setImageResource(R.drawable.ic_award_punch);
                }
            }catch (Exception e){

            }
        }

    }


    }
