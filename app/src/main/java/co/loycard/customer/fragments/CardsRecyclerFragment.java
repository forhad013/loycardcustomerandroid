package co.loycard.customer.fragments;

import android.app.Dialog;
import android.app.SearchManager;
import android.content.Context;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.firebase.ui.auth.AuthUI;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import co.loycard.customer.CustomerActivity;
import co.loycard.customer.R;
import co.loycard.customer.adapters.LoyaltyCardsRecyclerAdapter;
import co.loycard.customer.data_models.LoyaltyCard;
import co.loycard.customer.data_models.LoyaltyOffer;
import co.loycard.customer.data_models.Vendor;
import co.loycard.customer.observables.RxFirebase;
import io.reactivex.Observable;
import io.reactivex.ObservableSource;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.Action;
import io.reactivex.functions.Consumer;
import io.reactivex.functions.Function;
import io.reactivex.functions.Function3;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by Sam on 20/05/2017.
 */

public class CardsRecyclerFragment extends Fragment implements CardsRecyclerClickListener.OnCardsRecyclerClickListener {
    private SearchView searchView;
    private static final String TAG = "CardsRecyclerFragment";
    private static final int RC_SIGN_IN = 123;

    public static final String EXTRA_CARD_ID = "CardID";

    public static final String FRAGMENT_TAG = "CR";

    // Adapter for recycler view
    private LoyaltyCardsRecyclerAdapter mRecyclerAdapter;

    // Firebase Authentication
    private FirebaseAuth mFirebaseAuth;
    private FirebaseAuth.AuthStateListener mAuthStateListener;

    // Firebase Database
    private DatabaseReference mRootRef;
    private DatabaseReference mLoyaltyCardsRef;
    private ValueEventListener mValueEventListener;
    private Query mQuery;

    private List<LoyaltyCard> mCards;

    // ProgressBar
    private ProgressBar spinner;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Firebase initialisations
        mFirebaseAuth = FirebaseAuth.getInstance();
        mRootRef = FirebaseDatabase.getInstance().getReference();
        mLoyaltyCardsRef = mRootRef.child("LoyaltyCards");

        // Initialise cards list
        mCards = new ArrayList<>();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_cards_recycler, container, false);
        view.setTag(TAG);


        // Find recycler and set layout manager to linear layout manager
        RecyclerView recyclerView = (RecyclerView) view.findViewById(R.id.recycler_myCards);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));

        // Add click listener
        recyclerView.addOnItemTouchListener(new CardsRecyclerClickListener(getContext(), recyclerView, this));

        // create recycler adapter and set as adapter for cards recycler
        mRecyclerAdapter = new LoyaltyCardsRecyclerAdapter(mCards);
        recyclerView.setAdapter(mRecyclerAdapter);

        spinner = (ProgressBar)view.findViewById(R.id.card_spinner);
        spinner.setVisibility(View.VISIBLE);

        setHasOptionsMenu(true);


        Toolbar toolbar = (Toolbar) getActivity().findViewById(R.id.toolbar);

        TextView title =  (TextView) toolbar.findViewById(R.id.appTitle);
        ImageView icon =  (ImageView) toolbar.findViewById(R.id.logo);

        title.setText("My Cards");
        icon.setVisibility(View.VISIBLE);

        // Firebase Auth confirmation
        mAuthStateListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                FirebaseUser user = firebaseAuth.getCurrentUser();
                if (user != null) {
                    onSignedInInitailise(user);
                } else {
                    onSignedOutCleanup();
                    startActivityForResult(
                            AuthUI.getInstance()
                                    .createSignInIntentBuilder()
                                    .setProviders(Arrays.asList(new AuthUI.IdpConfig.Builder(AuthUI.EMAIL_PROVIDER).build(),
                                            new AuthUI.IdpConfig.Builder(AuthUI.GOOGLE_PROVIDER).build()))
                                    .setTheme(R.style.AuthTheme) //set bizName theme for Firebase UI here
                                    .build(),
                            RC_SIGN_IN);
                }
            }
        };
        return view;
    }







    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        getActivity().getMenuInflater().inflate(R.menu.menu_main,   menu);

        // Associate searchable configuration with the SearchView
        SearchManager searchManager = (SearchManager) getActivity().getSystemService(Context.SEARCH_SERVICE);
        searchView = (SearchView) menu.findItem(R.id.action_search)
                .getActionView();
      //  searchView.setSearchableInfo(searchManager
          //      .getSearchableInfo(getComponentName()));
        searchView.setMaxWidth(Integer.MAX_VALUE);

        // listening to search query text change
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                // filter recycler view when query submitted
                //Log.e("query",query);
                mRecyclerAdapter.getFilter().filter(query);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String query) {
                // filter recycler view when text is changed
                //Log.e("query",query);
                mRecyclerAdapter.getFilter().filter(query);
                return false;
            }
        });


        super.onCreateOptionsMenu(menu, inflater);
    }

    private void onSignedInInitailise(FirebaseUser user) {
        attachDatabaseListener();
        Log.d(TAG, "onSignedInIitailise: MyCardsActivity, UID = " + user.getUid());
    }




    private void onSignedOutCleanup() {
        mCards.clear();
        mRecyclerAdapter.setCards(mCards);
        detachDatabaseReadListener();
    }

    // Attaches the database listener to trigger callback when Firebase data changes
    private void attachDatabaseListener() {
        mQuery = mLoyaltyCardsRef.orderByChild("customerID").equalTo(mFirebaseAuth.getCurrentUser().getUid());

        if (mValueEventListener == null) {
            mValueEventListener = new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    if (dataSnapshot.exists()) {
                        Log.d(TAG, "onDataChange: data change detected");

                        // Create new array list so the cards displayed don't change until all data available
                        final ArrayList<LoyaltyCard> cards = new ArrayList<>();
                        //mCards.clear();

                        for (DataSnapshot cardSnapshot : dataSnapshot.getChildren()) {
                            LoyaltyCard card = cardSnapshot.getValue(LoyaltyCard.class);
                            card.setCardID(cardSnapshot.getKey());
                            cards.add(card); //TODO check valid
                        }


                        populateRecycler(cards);
                    } else {
                        spinner.setVisibility(View.GONE);
                        showDialoge(true,"");
                        ((CustomerActivity)getActivity()).openHome();

                    }
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                    attachDatabaseListener();

                    Log.d(TAG, "onCancelled: database error occurred. Details: " + databaseError.getDetails() + ", Message: " + databaseError.getMessage());
                }
            };
        }
        mQuery.addValueEventListener(mValueEventListener);
    }

    public void showDialoge(boolean success,String text){
        final Dialog dialog = new Dialog(getActivity());
        // Include dialog.xml file
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_layout);
        // Set dialog title
        //dialog.setTitle("Category Add");
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.show();

        Button done = (Button) dialog.findViewById(R.id.done);

        TextView title = (TextView) dialog.findViewById(R.id.title);

        TextView message = (TextView) dialog.findViewById(R.id.message);

        Typeface custom_font = Typeface.createFromAsset(getActivity().getAssets(),  "fonts/HelveticaNeueLTStd-Md.otf");

        title.setTypeface(custom_font);
        message.setTypeface(custom_font);
        done.setTypeface(custom_font);


        message.setText("Nothing to show here");
        title.setText("Oh snap!");

        //   Log.e("text",text);

        done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });



    }
    // Detaches the database listener
    private void detachDatabaseReadListener() {
        if (mValueEventListener != null) {
            mQuery.removeEventListener(mValueEventListener);
            mValueEventListener = null;
        }
    }

    private void populateRecycler(final ArrayList<LoyaltyCard> cards) {
        // Create an observable stream from the cards retrieved
        Observable.fromIterable(cards) //TODO check valid
                .observeOn(Schedulers.io())
                .flatMap(new Function<LoyaltyCard, ObservableSource<?>>() {
                    @Override
                    public ObservableSource<?> apply(@io.reactivex.annotations.NonNull LoyaltyCard loyaltyCard) throws Exception {
                        // create two new streams to get the vendor and offer
                        Observable<Vendor> ven = RxFirebase.getVendor(mRootRef, loyaltyCard.vendorID);
                        Observable<LoyaltyOffer> off = RxFirebase.getLoyaltyOffer(mRootRef, loyaltyCard.offerID);
                        Observable<LoyaltyCard> card = Observable.just(loyaltyCard);
                        Function3<Vendor, LoyaltyOffer, LoyaltyCard, LoyaltyCard> f = new Function3<Vendor, LoyaltyOffer, LoyaltyCard, LoyaltyCard>() {
                            @Override
                            public LoyaltyCard apply(@io.reactivex.annotations.NonNull Vendor vendor, @io.reactivex.annotations.NonNull LoyaltyOffer offer, @io.reactivex.annotations.NonNull LoyaltyCard loyaltyCard) throws Exception {
                                loyaltyCard.setBusinessName(vendor.businessName);
                                loyaltyCard.setOfferDescription(offer.description);

                                return loyaltyCard;
                            }
                        };
                        // zip the two observable streams together and returns a new observable
                        Observable<LoyaltyCard> observable = Observable.zip(ven, off, card, f);
                        return observable;
                    }
                }).retry(3)
                .doOnError(new Consumer<Throwable>() {
                    @Override
                    public void accept(@io.reactivex.annotations.NonNull Throwable throwable) throws Exception {
                        Log.d(TAG, "MyCardsActivity: Observable from iterable: " + throwable.getMessage());
                    }
                }).onErrorReturnItem(new LoyaltyCard())
                .observeOn(AndroidSchedulers.mainThread())
//                /* To help with understanding functionality, uncomment this code. This is how you would trigger behaviour
//                   when each card was available. Here it just changes the business name to "Debug Test" but could also
//                   be used to populate recycler gradually as cards were available */
//                .doOnNext(new Consumer<Object>() {
//                    @Override
//                    public void accept(@io.reactivex.annotations.NonNull Object loyaltyCard) throws Exception {
//                        ((LoyaltyCard) loyaltyCard).setBusinessName("Debug Test");
//                    }
//                })
                .doOnComplete(new Action() {
                    @Override
                    public void run() throws Exception {
                        // set the cards to the recycler adapter
                        mCards = cards;
                        mRecyclerAdapter.setCards(mCards);
                        spinner.setVisibility(View.GONE);
                    }
                })
                .subscribe();
                spinner.setVisibility(View.GONE);
    }

    @Override
    public void onClick(View view, int position) {
        FragmentManager manager = getFragmentManager();
        manager.beginTransaction()
                .replace(R.id.content, CardDetailsFragment.newInstance(mCards.get(position).retrieveCardID()))
                .addToBackStack(null)
                .commit();
    }

    @Override
    public void onResume() {
        super.onResume();
        Toolbar toolbar = (Toolbar) getActivity().findViewById(R.id.toolbar);

        TextView title =  (TextView) toolbar.findViewById(R.id.appTitle);

        title.setText("My Cards");
        ImageView icon =  (ImageView) toolbar.findViewById(R.id.logo);
        icon.setVisibility(View.VISIBLE);
        // Add the firebase auth state listener
        mFirebaseAuth.addAuthStateListener(mAuthStateListener);
    }

    @Override
    public void onPause() {
        super.onPause();

        // Remove the firebase auth state listener
        mFirebaseAuth.removeAuthStateListener(mAuthStateListener);
    }
}
