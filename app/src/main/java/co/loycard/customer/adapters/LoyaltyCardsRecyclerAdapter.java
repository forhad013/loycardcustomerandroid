package co.loycard.customer.adapters;

import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.util.ArrayList;
import java.util.List;

import co.loycard.customer.R;
import co.loycard.customer.data_models.LoyaltyCard;

import static com.facebook.FacebookSdk.getApplicationContext;

/**
 * RecyclerView Adapter for the LoyaltyCards Recycler on the starting activity. This follows the standard
 * structure for a recycler adapter
 * Created by Sam on 26/04/2017.
 */

public class LoyaltyCardsRecyclerAdapter extends RecyclerView.Adapter<LoyaltyCardsRecyclerAdapter.LoyaltyCardViewHolder>   implements Filterable {
    private static final String TAG = "LoyaltyCardsRecyclerAda";

    private List<LoyaltyCard> mCards;
    private List<LoyaltyCard> mCardsFiltered;
    private StorageReference mStorage;
    public LoyaltyCardsRecyclerAdapter(List<LoyaltyCard> cards) {
        mCards = cards;
    }

    public void setCards(List<LoyaltyCard> cards) {
        mCardsFiltered = cards;
        notifyDataSetChanged();
        mCards = cards;
        mStorage = FirebaseStorage.getInstance().getReference();
    }

    @Override
    public LoyaltyCardViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.loyalty_card_list_item, parent, false);
        LoyaltyCardViewHolder holder = new LoyaltyCardViewHolder(view);
        return  holder;
    }

    @Override
    public void onBindViewHolder(final LoyaltyCardViewHolder holder, int position) {
        LoyaltyCard card = mCardsFiltered.get(position);
        holder.businessName.setText(card.retrieveBusinessName());
        holder.offerDescription.setText(card.retrieveOfferDescription());
        holder.purchaseCount.setText("Purchases: " + card.purchaseCount);

        try {
            mStorage.child("Images/" + card.offerID.toString() + "/" + card.vendorID.toString() + "/").getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                @Override
                public void onSuccess(Uri uri) {
                    Glide.with(getApplicationContext()).load(uri).override(100, 100).error(R.drawable.placeholder).into(holder.imageview);

                }
            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception exception) {
                    Glide.with(getApplicationContext()).load(R.drawable.placeholder).override(100, 100).into(holder.imageview);
                }
            });
        }catch (Exception e){

        }
    }

    @Override
    public int getItemCount() {
        return ((mCardsFiltered != null) && (mCardsFiltered.size() != 0)) ? mCardsFiltered.size() : 0;
    }

    public LoyaltyCard getCard(int position) {
        return ((mCardsFiltered != null) && (mCardsFiltered.size() != 0)) ? mCardsFiltered.get(position) : null;
    }

    static class LoyaltyCardViewHolder extends RecyclerView.ViewHolder {
        TextView businessName = null;
        TextView offerDescription = null;
        ImageView imageview = null;
        TextView purchaseCount = null;

        public LoyaltyCardViewHolder(View itemView) {
            super(itemView);
            this.businessName = (TextView) itemView.findViewById(R.id.text_businessName);
            this.offerDescription = (TextView) itemView.findViewById(R.id.text_offerDescription);
            this.purchaseCount = (TextView) itemView.findViewById(R.id.text_purchaseCount);
            this.imageview = (ImageView) itemView.findViewById(R.id.image);
        }
    }


    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();
                 //Log.e("charString",charString);
                if (charString.isEmpty()) {
                    mCardsFiltered = mCards;
                } else {
                    List<LoyaltyCard> filteredList = new ArrayList<>();
                    for (LoyaltyCard row : mCards) {

                        //Log.e("test",row.retrieveOfferDescription());
                        // name match condition. this might differ depending on your requirement
                        // here we are looking for name or phone number match
                        if (row.retrieveOfferDescription().toLowerCase().contains(charString.toLowerCase()) ||row.retrieveBusinessName().toLowerCase().contains(charString.toLowerCase())) {

                            //Log.e("row",row.retrieveOfferDescription().toLowerCase());
                            //Log.e("char",charString.toLowerCase());
                            filteredList.add(row);
                        }
                    }

                    mCardsFiltered = filteredList;

                }



                FilterResults filterResults = new FilterResults();
                filterResults.values = mCardsFiltered;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                mCardsFiltered = (ArrayList<LoyaltyCard>) filterResults.values;

                notifyDataSetChanged();
            }
        };
    }

}
