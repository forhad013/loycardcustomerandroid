package co.loycard.customer;

import android.app.Dialog;
import android.graphics.Bitmap;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;
import com.journeyapps.barcodescanner.BarcodeEncoder;

import java.util.ArrayList;

import co.loycard.customer.data_models.LoyaltyCard;

public class RewardActivity extends AppCompatActivity {

    ImageButton bckBtn;
    ImageView qrImageView;
    TextView dealNameTv, vendorNameTv,vendorAddressTv;
    String dealNameString, vendorNameString,vendorAddressString,dealId,token,offerID_customerID;

    private ValueEventListener mValueEventListener;
    private Query mQuery;
    private DatabaseReference mRootRef;
    private DatabaseReference mLoyaltyCardsRef;
    private DatabaseReference mLoyaltyReward;

    String mUserID;
    private FirebaseAuth mFirebaseAuth;
    FirebaseUser mFirebaseUser;

    int purchaseCount=0,newPurchaseCount=0;
    int rewardCount=0,newRewardCount=0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reward);

        bckBtn = (ImageButton) findViewById(R.id.bck_btn);

        mFirebaseAuth = FirebaseAuth.getInstance();

        mFirebaseUser = mFirebaseAuth.getCurrentUser();


        mRootRef = FirebaseDatabase.getInstance().getReference();
        mLoyaltyCardsRef = mRootRef.child("LoyaltyCards");


        dealNameTv = (TextView) findViewById(R.id.dealName);
        vendorNameTv = (TextView) findViewById(R.id.vendorName);
        vendorAddressTv = (TextView) findViewById(R.id.vendorAddress);
        qrImageView = (ImageView) findViewById(R.id.qrImageView);

        dealNameString = getIntent().getStringExtra("dealNameString");
        vendorNameString = getIntent().getStringExtra("vendorNameString");
        vendorAddressString = getIntent().getStringExtra("vendorAddressString");
        offerID_customerID = getIntent().getStringExtra("offerID_customerID");
        purchaseCount = getIntent().getIntExtra("purchaseCount",0);
        rewardCount = getIntent().getIntExtra("rewardCount",0);
        token = getIntent().getStringExtra("token");


    //    Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);

        TextView  title =  (TextView) findViewById(R.id.appTitle);

       // if(token.equalsIgnoreCase("Punch Now")){
            title.setText("Punch");
     //   }else{
        //    title.setText("Redeem");
       // }



        Log.e("rewardCount",rewardCount+" s");

        dealNameTv.setText(dealNameString);
        vendorNameTv.setText(vendorNameString);
        vendorAddressTv.setText(vendorAddressString);
        onSignedInInitialise(mFirebaseUser);

       // showDialoge(true,"");

        Log.e("offerID_customerID",offerID_customerID+"");

        bckBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });



        attachDatabaseListenerPunch();


    }


    private void onSignedInInitialise(FirebaseUser user) {
        // Get Firebase User ID
        mUserID = user.getUid();



        // Set ImageView to QR Code
        MultiFormatWriter multiFormatWriter = new MultiFormatWriter();
        try {
            BitMatrix bitMatrix = multiFormatWriter.encode(mUserID, BarcodeFormat.QR_CODE, 400, 400);
            BarcodeEncoder barcodeEncoder = new BarcodeEncoder();
            Bitmap bitmap = barcodeEncoder.createBitmap(bitMatrix);
            qrImageView.setImageBitmap(bitmap);
        } catch (WriterException e) {
            e.printStackTrace();
        }

        // associate deviceID with customerID to receive vendor specific push notifications

    }


    public void showDialoge(boolean success,String text){
        final Dialog dialog = new Dialog(RewardActivity.this);
        // Include dialog.xml file
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_layout);
        // Set dialog title
        //dialog.setTitle("Category Add");
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.show();

        Button done = (Button) dialog.findViewById(R.id.done);

        TextView title = (TextView) dialog.findViewById(R.id.title);

        TextView message = (TextView) dialog.findViewById(R.id.message);

        message.setText(text);

     //   Log.e("text",text);

        done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });



    }

    private void attachDatabaseListenerPunch() {
        mQuery = mLoyaltyCardsRef.orderByChild("offerID_customerID").equalTo(offerID_customerID);

       // mQuery = mLoyaltyCardsRef.orderByChild("customerID").equalTo(mFirebaseAuth.getCurrentUser().getUid());

        if (mValueEventListener == null) {
            mValueEventListener = new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    if (dataSnapshot.exists()) {
                        Log.d("QrAc", "onDataChange: data change detected");
                        Log.d("QrAc",dataSnapshot.getChildren().toString());

                        // Create new array list so the cards displayed don't change until all data available
                        final ArrayList<LoyaltyCard> cards = new ArrayList<>();
                        //mCards.clear();

                        for (DataSnapshot cardSnapshot : dataSnapshot.getChildren()) {
                            LoyaltyCard card = cardSnapshot.getValue(LoyaltyCard.class);
                            card.setCardID(cardSnapshot.getKey());
                            cards.add(card); //TODO check valid

                            newPurchaseCount = Integer.parseInt(card.purchaseCount);
                            newRewardCount = Integer.parseInt(card.rewardsIssued);

//                            Log.d("newPurchaseCount",newPurchaseCount+"");
//                            Log.d("purchaseCount",purchaseCount+"");
//                            Log.d("newR",newRewardCount+"");
//                            Log.d("R",rewardCount+"");
                        }
                        if(newPurchaseCount!=purchaseCount) {
                          //  if(newRewardCount!=rewardCount) {
                           //     showDialoge(true, "Congratulation Reward Successfully Redeemed");
                         //   }else {
                                showDialoge(true, "Purchase Count Successfully Updated");
                         //   }
                        }

                    } else {

                    }
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {
                    Log.d("QrA", "onCancelled: database error occurred. Details: " + databaseError.getDetails() + ", Message: " + databaseError.getMessage());
                }
            };
        }
        mQuery.addValueEventListener(mValueEventListener);
    }

    private void attachDatabaseListenerReedme() {
        mQuery = mLoyaltyCardsRef.orderByChild("offerID_customerID").equalTo(offerID_customerID);

       // mQuery = mLoyaltyCardsRef.orderByChild("customerID").equalTo(mFirebaseAuth.getCurrentUser().getUid());

        if (mValueEventListener == null) {
            mValueEventListener = new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    if (dataSnapshot.exists()) {
                        Log.d("QrAc", "onDataChange: data change detected");
                        Log.d("QrAc",dataSnapshot.getChildren().toString());

                        // Create new array list so the cards displayed don't change until all data available
                        final ArrayList<LoyaltyCard> cards = new ArrayList<>();
                        //mCards.clear();

                        for (DataSnapshot cardSnapshot : dataSnapshot.getChildren()) {
                            LoyaltyCard card = cardSnapshot.getValue(LoyaltyCard.class);
                            card.setCardID(cardSnapshot.getKey());
                            cards.add(card); //TODO check valid

                            newPurchaseCount = Integer.parseInt(card.purchaseCount);
                            newRewardCount = Integer.parseInt(card.rewardsIssued);

                            Log.d("newPurchaseCount",newPurchaseCount+"");
                            Log.d("purchaseCount",purchaseCount+"");
                            Log.d("newR",newRewardCount+"");
                            Log.d("R",rewardCount+"");
                        }
                        if(newPurchaseCount!=purchaseCount) {
                            showDialoge(true, "Purchase Count Successfully Updated");
                        }else if(newRewardCount!=rewardCount) {
                            showDialoge(true, "Reward Successfully Reedmed");
                        }


                    } else {

                    }
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {
                    Log.d("QrA", "onCancelled: database error occurred. Details: " + databaseError.getDetails() + ", Message: " + databaseError.getMessage());
                }
            };
        }
        mQuery.addValueEventListener(mValueEventListener);
    }
}
