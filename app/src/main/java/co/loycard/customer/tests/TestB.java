package co.loycard.customer.tests;

import org.junit.Assert;
import org.junit.Test;

import co.loycard.customer.TestsActivity;

/**
 * See description for test A for explanation
 *
 * @see <bizName href="http://d.android.com/tools/testing">Testing documentation</bizName>
 */
public class TestB {

    @Test
    public void test() {
        Assert.assertTrue(TestsActivity.mTestA_Passed);
    }

}