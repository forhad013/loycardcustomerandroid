package co.loycard.customer;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.firebase.ui.auth.AuthUI;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import java.util.ArrayList;

import co.loycard.customer.fragments.CardsRecyclerFragment;
import co.loycard.customer.fragments.PromotionsFragment;
import co.loycard.customer.fragments.QrFragment;
import co.loycard.customer.fragments.RewardsRecyclerFragment;

public class CustomerActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    private static final String TAG = "CustomerActivity";

    // Navigation Drawer Objects
    private NavigationView nView;
    private DrawerLayout drawerLayout;
    private ActionBarDrawerToggle drawerToggle;

    // User Info TextViews
    private TextView userNameView;
    private TextView emailView;

    boolean shoudlShowName = true;

    TextView title;
    ImageView appIcon;
    FirebaseUser user;
    // Firebase Authentication
    private FirebaseAuth mFirebaseAuth;
    private FirebaseAuth.AuthStateListener mAuthStateListener;
    RelativeLayout relativeLayout;
      Toolbar toolbar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_customer);

        // Navigation Drawer
         toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        appIcon =  (ImageView) toolbar.findViewById(R.id.logo);


        Log.e("eloa","e");

        drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawerToggle = new ActionBarDrawerToggle(
                this,
                drawerLayout,
                toolbar,
                R.string.navigation_drawer_open,
                R.string.navigation_drawer_close);
        drawerLayout.addDrawerListener(drawerToggle);
        drawerToggle.syncState();
        nView = (NavigationView) findViewById(R.id.nav_view);
        nView.setNavigationItemSelectedListener(this);


        title =  (TextView) toolbar.findViewById(R.id.appTitle);

        relativeLayout =  (RelativeLayout) toolbar.findViewById(R.id.rel);
        // Firebase Authentication Initialisation
        mFirebaseAuth = FirebaseAuth.getInstance();

        // Firebase UI Authentication
        mAuthStateListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                  user = firebaseAuth.getCurrentUser();
                if (user != null) {
                    // User is signed in
                    Log.d(TAG, "onAuthStateChanged: is signed_in:" + user.getUid());

                    // User Info TextViews
                    userNameView = (TextView) nView.getHeaderView(0).findViewById(R.id.username_view);
                    emailView = (TextView) nView.getHeaderView(0).findViewById(R.id.email_view);
                    userNameView.setText(user.getDisplayName());
                    emailView.setText(user.getEmail());
                    getSupportActionBar().setDisplayShowTitleEnabled(false);
                  //  title.setText(user.getDisplayName());


//                    relativeLayout.setVisibility(View.GONE);
//                    toolbar.setTitle(user.getDisplayName());
                  //  getSupportActionBar().setDisplayShowTitleEnabled(false);

                } else {
                    getSupportActionBar().setDisplayShowTitleEnabled(false);
                    // User is signed out
                    Log.d(TAG, "onAuthStateChanged: is signed_out");
                }
            }
        };


        // TODO check if launched from notification with Extra describing fragment to launch

        // Set QrFragment as the initial fragment
        if (savedInstanceState == null) {
            Fragment frag = new QrFragment();
            FragmentManager manager = getSupportFragmentManager();
            manager.beginTransaction()
                    .replace(R.id.content, frag, QrFragment.FRAGMENT_TAG)
                    .commit();


        }

    }

   public void openHome(){
       Fragment frag = new QrFragment();
       FragmentManager manager = getSupportFragmentManager();
       manager.beginTransaction()
               .replace(R.id.content, frag, QrFragment.FRAGMENT_TAG)
               .commit();
    }
   public void openReward(){
       Fragment frag = new RewardsRecyclerFragment();
       FragmentManager manager = getSupportFragmentManager();
       manager.beginTransaction()
               .replace(R.id.content, frag, RewardsRecyclerFragment.FRAGMENT_TAG)
               .commit();
    }

    // On resuming activity
    @Override
    protected void onResume() {

        super.onResume();

        // Add the firebase auth state listener
        try {
            mFirebaseAuth.addAuthStateListener(mAuthStateListener);
        }catch (Exception e){

        }
    }

    // On pausing activity
    @Override
    protected void onPause() {

        super.onPause();

        // Remove the firebase auth state listener
        try {
            mFirebaseAuth.removeAuthStateListener(mAuthStateListener);
        }catch (Exception e){

        }
    }

    // When back is pressed the drawer must close if open
    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }


    // Called when a navigation drawer item is selected
    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {

        // Get fragment manager
        FragmentManager manager = getSupportFragmentManager();

        // Fragment tag. This must be set when item is selected
        String tag = "";

        // Declare variable for current fragment
        Fragment current = null;

        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_home) {

            shoudlShowName = true;
            // check if fragment exists already
            tag = QrFragment.FRAGMENT_TAG;
            current = manager.findFragmentByTag(tag);
            if (current == null) {
                // create if doesn't exist
                current = new QrFragment();

                if (user != null) {

                    title.setText(user.getDisplayName());
                    appIcon.setVisibility(View.GONE);

                }
            }

        } else if (id == R.id.nav_myCards) {
            shoudlShowName = false;
            // check if fragment exists already
            tag = CardsRecyclerFragment.FRAGMENT_TAG;
            current = manager.findFragmentByTag(tag);
            if (current == null) {
                // create if doesn't exist
                current = new CardsRecyclerFragment();

                title.setText("My Cards");
               appIcon.setVisibility(View.VISIBLE);
            }

            //title.setText(user.getDisplayName());

        } else if (id == R.id.nav_rewards) {
            shoudlShowName = false;
            // check if fragment exists already
            tag = RewardsRecyclerFragment.FRAGMENT_TAG;
            current = manager.findFragmentByTag(tag);
            if (current == null) {
                // create if doesn't exist
                current = new RewardsRecyclerFragment();

                title.setText("Rewards");
                appIcon.setVisibility(View.VISIBLE);
            }
        }else if (id == R.id.nav_promotions) {
            shoudlShowName = false;
            tag = PromotionsFragment.FRAGMENT_TAG;
            current = manager.findFragmentByTag(tag);
            if (current == null) {
                // create if doesn't exist
                current = new PromotionsFragment();
                title.setText("Promotion");
                appIcon.setVisibility(View.VISIBLE);
            }

        } else if (id == R.id.nav_signout) {
            // Firebase sign out
            shoudlShowName = false;
            openHome();
            AuthUI.getInstance().signOut(this);
//            startActivity(new Intent(this,CustomerActivity.class));
//            finish();

        }

        if (current == null) {
            drawerLayout.closeDrawer(GravityCompat.START);
            return true;
        }

        // Fragment manager transaction to replace currently displayed fragment with new currently selected fragment
        manager.beginTransaction()
                .replace(R.id.content, current, tag)
              //  .addToBackStack(tag)
                .commit();
        drawerLayout.closeDrawer(GravityCompat.START);

        return true;
    }





}
